public class Fibonacci {
    public static void main(String[] args) {
        int count = 11;
        System.out.println("Print first " + count + " Fibonacci elements by for cycle:");
        FibonacciFor(count);
        System.out.println("\nPrint first " + count + " Fibonacci elements by while cycle:");
        FibonacciWhile(count);
    }

    private static void FibonacciFor(int number) {
        int first = 1;
        int second = 1;
        int next;
        for (int i = 0; i < number; i++) {
            System.out.println((i + 1) + ": " + first);
            next = first + second;
            first = second;
            second = next;
        }
    }

    private static void FibonacciWhile(int number) {
        int first = 1;
        int second = 1;
        int i = 0;
        int next;
        while (i < number) {
            System.out.println((i + 1) + ": " + first);
            next = first + second;
            first = second;
            second = next;
            i++;
        }
    }

}
